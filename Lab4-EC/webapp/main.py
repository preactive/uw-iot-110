import time
from bmp280 import *
from flask import *



app = Flask(__name__)

@app.route("/")
def index():
    return render_template('index.html')


# =========================== Endpoint: /myData ===============================
# read the gpio states by GET method from curl for example
# curl http://iot8e3c:5000/myData
# -----------------------------------------------------------------------------
@app.route('/myData')
def myData():
    def get_state_values():
        while True:
            # return the yield results on each loop, but never exits while loop
            chip_id, chip_version, temperature, pressure = main()
            temperature = int((temperature * 1.8)  + 32)
            pressure = int(pressure)
            yield('data: {0} {1} {2} {3}\n\n'.format(chip_id,chip_version,temperature,pressure))
            time.sleep(.1)
    return Response(get_state_values(), mimetype='text/event-stream')


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, threaded=True)